jQuery.sap.declare("utils.Formatter");
jQuery.sap.require("utils.ParseDate");

utils.Formatter = {

    formatPriority :  function (p) {
			if (p === "Normal") {
				return "Success";
			} else if (p === "Warning") {
				return "Warning";
			} else if (p === "Urgent"){
				return "Error";
			} else {
				return "None";
			}
    },
    
    isSelected: function (obj) {
        if (obj !== "")
          return true;
        return false;

    },



};
