jQuery.sap.declare("model.ui.LogoTile");
jQuery.sap.require("sap.m.library");
jQuery.sap.require("sap.m.StandardTile");

jQuery.sap.includeStyleSheet("custom/model/ui/css/LogoTile.css","logoTileCSSid");

sap.m.StandardTile.extend
(
	"model.ui.LogoTile",
	{
		metadata :
		{
			properties : {           // setter and getter are created behind the scenes, incl. data binding and type validation
				                     // in simple cases, just define the type
				"imgUrl" : "string",
				"text" : "string",
                "color" : "string",
                "backgroundColor" : "string"
				//"subText" : "string",
//				"bottomImg" : "string"
                
			}
		},

		renderer: {
			_renderContent: function(r, t)
			{
				r.write('<img class="LogoTileImg" src="'+ t.getImgUrl() +'" alt="">');
				r.write('<div class="LogoTileText"><span style="color:' + t.getColor() +'">' + t.getText() + ' </span></div>');
				//r.write('<div class="LogoTileSubText"><p>' + t.getSubText() + '</p></div>');
//				r.write('<img class="LogoTileBottomImg" src="'+ t.getBottomImg() +'" alt="">');
			},
		},
        
        onAfterRendering: function(oEvent) {  
       // !!! important: we have to call the original onAfterRendering method to get the tiles placed properly !!!  
       sap.m.StandardTile.prototype.onAfterRendering.apply(this, arguments);  
       var oTile = oEvent.srcControl;  
       // get dom element and add style commands to display an background-image  
       var oDOMEl = document.getElementById(oTile.getId());  
       if (oDOMEl) {  
            //oDOMEl.style.backgroundImage="url('" + this.getBackgroundImage() + "')";
            //oDOMEl.style.backgroundColor = this.getBackgroundColor();
            oDOMEl.childNodes[1].style.backgroundColor=this.getBackgroundColor();
            oDOMEl.style.backgroundRepeat="no-repeat";  
            oDOMEl.style.backgroundSize="contain"; // !!! CSS3 !!!  
       }  
      },  
      init: function() {  
           sap.m.StandardTile.prototype.init.apply(this, arguments);  
      } 
	}
);


 