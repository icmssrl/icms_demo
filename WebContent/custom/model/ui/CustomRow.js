jQuery.sap.declare("model.ui.CustomRow");
jQuery.sap.require("sap.m.library");
jQuery.sap.require("sap.m.ColumnListItem");

//jQuery.sap.includeStyleSheet("custom/model/ui/css/LogoTile.css","id");

sap.m.ColumnListItem.extend
(
	"model.ui.CustomRow",
	{
		metadata :
		{
			properties : {           // setter and getter are created behind the scenes, incl. data binding and type validation
				                     // in simple cases, just define the type
                "backgroundColor" : "string"

                
			}
            
		},
        
        renderer: {
			
		},

        
        onAfterRendering: function(oEvent) {  
       // !!! important: we have to call the original onAfterRendering method to get the tiles placed properly !!!  
       sap.m.ColumnListItem.prototype.onAfterRendering.apply(this, arguments);  
       var oRow = oEvent.srcControl;  
       // get dom element and add style commands to display an background-image  
       var oDOMEl = document.getElementById(oRow.getId());  
       if (oDOMEl) {  
            
            oDOMEl.style.backgroundColor = this.getBackgroundColor();
             
       }  
      },  
      init: function() {  
           sap.m.ColumnListItem.prototype.init.apply(this, arguments);  
      } 
	}
);