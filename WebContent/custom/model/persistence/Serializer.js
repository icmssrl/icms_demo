jQuery.sap.declare("model.persistence.Serializer");

model.persistence.Serializer = {

ticket:{


    fromSAP: function(sapData)
    {
      var o = {};
//        var arraynote = [];
        o.ticketId = sapData.ticketId ? sapData.ticketId : "";
        o.description = sapData.description ? sapData.description : "";
        o.priority = sapData.priority ? sapData.priority : "";
        o.status = sapData.status ? sapData.status : "";
        o.reportedBy = sapData.reportedBy ? sapData.reportedBy : "";
        o.supportTeam = sapData.supportTeam ? sapData.supportTeam : "";
        o.recipient = sapData.recipient ? sapData.recipient : "";
        o.ticketType = sapData.ticketType ? sapData.ticketType : "";
        o.deliveryDate = sapData.deliveryDate ? sapData.deliveryDate : "";
        o.deliveryDateCounterStop = sapData.deliveryDateCounterStop ? sapData.deliveryDateCounterStop : "";
        o.nextStates = sapData.nextStates ? sapData.nextStates : [];
        o.notes = sapData.notes ? sapData.notes : [];
//        for(var i = 0; i< (o.notes).length; i++)
//        {
//            (o.notes)[i].text = 
//        }
        o.docs = sapData.docs ? sapData.docs : [];
        o.associatedPartners = sapData.associatedPartners ? sapData.associatedPartners : [];
     
      return o;
    },
    
    toSAP: function(obj)
    {
       var toSAPData = {};
     
      return toSAPData; 
    }
    },
    
    partner:{


    fromSAP: function(sapData)
    {
      var p = {};

        p.partnerId = sapData.partnerId ? sapData.partnerId : "";
        p.partnerName = sapData.partnerName ? sapData.partnerName : "";
        
      return p;
    },
    
    toSAP: function(obj)
    {
       var toSAPData = {};
     
      return toSAPData; 
    }
    }
  

};
