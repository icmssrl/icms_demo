jQuery.sap.declare("model.Ticket");

model.Ticket = (function () {

  Ticket = function (serializedData) {
    
    this.ticketId = undefined;
    this.description = undefined;
    this.priority = undefined;
    this.status = undefined;
    this.reportedBy = undefined;
    this.supportTeam = undefined;
    this.recipient = undefined;
    this.ticketType = undefined;
    this.deliveryDate = undefined;
    this.deliveryDateStopCounter = undefined;
    this.nextStates = [];
    this.notes = [];
    this.docs = [];
    this.associatedPartners = [];
    

    this.getId = function () {
      return this.ticketId;
    };
      
    this.getDescription = function () {
      return this.description;
    };
      
    this.getNotes = function () {
      return this.notes;
    };
      
    this.getDocs = function () {
      return this.docs;
    };
      
    this.getNextStates = function () {
      return this.nextStates;
    };
      
    this.getAssociatedPartners = function () {
      return this.associatedPartners;
    };
      
    this.setDescription = function (description) {
      this.description = description;
    };
      
    this.setNotes = function (notes) {
        
      this.notes = notes;
    };
      
    this.setDocs = function (docs) {
      this.docs = docs;
    };
      
    this.setAssociatedPartners = function (newPartners) {
      this.associatedPartners = newPartners;
    };
      
    this.setNextStates = function (newStates) {
      this.nextStates = newStates;
    };
      
    this.setNewStatus = function (newState) {
      this.status = newState;
    };
      
    this.addNewPartners = function (partners){
        if(this.associatedPartners.length>0){
            if(partners.length>0){
                for(var i = 0; i<partners.length; i++){
                    this.associatedPartners.push(partners[i]);
                }
            }
            
        }else{
            this.associatedPartners = partners;
        }
    },

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };


    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };

    if (serializedData) {
      this.update(serializedData);
    }
    return this;
  };
  return Ticket;


})();
