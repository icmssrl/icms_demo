
jQuery.sap.declare("model.collections.Partners");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Partner");


model.collections.Partners = ( function ()
{
  var partners = [];
  //var defer = Q.defer();

  return {

    getById : function(code)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var partner = _.find(result, _.bind(function(item)
        {
          return item.getId() === code;
        }, this));
        if(partner)
        {
          deferId.resolve(partner); //
        }
        else
        {
          console.log("Partners.js -- Partner not found!");
        }
        
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      };
      
      fError = _.bind(fError, this);

      this.loadPartners()
      .then(fSuccess, fError);

      return deferId.promise;
    },


    loadPartners : function()
    {
        var defer = Q.defer();
        
        if(defer && defer.promise.isFulfilled())
        {
          defer.resolve(partners);
        }
        else
        {
          var fSuccess = function(result)
          {
            if(result && result.length>0)
            {
              partners=[];
              var add = _.bind(this.addPartner, this);
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.partner.fromSAP(result[i]);
                add(new model.Partner(data));
              }

            }
            defer.resolve(partners);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            partners  = [];
            console.log("Error loading Partners!");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/partners.json")
            .success(fSuccess)
            .fail(fError);
        }
        // defer = Q.defer();



        return defer.promise;
    },
  
      
    addPartner : function(partner)
    {
      partners.push(partner);
    },
    //--------------------------------------------------------------------


    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"partners":[]};
      for(var i = 0; i< partners.length; i++)
      {
        data.partners.push(partners[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
