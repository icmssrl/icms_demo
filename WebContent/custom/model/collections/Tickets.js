
jQuery.sap.declare("model.collections.Tickets");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Ticket");


model.collections.Tickets = ( function ()
{
  var tickets = [];
  //var defer = Q.defer();

  return {

    getById : function(code)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var ticket = _.find(result, _.bind(function(item)
        {
          return item.getId() === parseInt(code);
        }, this));
        if(ticket)
        {
          deferId.resolve(ticket); //
        }
        else
        {
          console.log("Tickets.js -- Ticket not found!");
        }
        
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      };
      
      fError = _.bind(fError, this);

      this.loadTickets()
      .then(fSuccess, fError);

      return deferId.promise;
    },


    loadTickets : function()
    {
        var defer = Q.defer();
        
        if(defer && defer.promise.isFulfilled())
        {
          defer.resolve(tickets);
        }
        else
        {
          var fSuccess = function(result)
          {
            if(result && result.length>0)
            {
              tickets=[];
              var add = _.bind(this.addTicket, this);
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.ticket.fromSAP(result[i]);
                add(new model.Ticket(data));
              }

            }
            defer.resolve(tickets);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            tickets  = [];
            console.log("Error loading Tickets!");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/tickets.json")
            .success(fSuccess)
            .fail(fError);
        }
        // defer = Q.defer();



        return defer.promise;
    },
  
      
    addTicket : function(ticket)
    {
      tickets.push(ticket);
    },
    //--------------------------------------------------------------------


    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"tickets":[]};
      for(var i = 0; i< tickets.length; i++)
      {
        data.tickets.push(tickets[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
