jQuery.sap.declare("model.odata.chiamateOdata");
jQuery.sap.require("icms.Settings");
jQuery.sap.require("icms.Component");

model.odata.chiamateOdata = {
    _serviceUrl : icms.Component.getMetadata().getConfig().settings.serverUrl,


    getOdataModel : function(){
        if(!this._odataModel){
            this._odataModel = new sap.ui.model.odata.ODataModel(this._serviceUrl, true);
        }
        return this._odataModel;
    },

    getUserById: function (id,success, error) {
        this.getOdataModel().read("XF_User_DataSet?$filter=Uname eq '"+id+"'", {success: success, error: error, async: true});
    },


    login: function(user, pwd, success, error){
      this.getOdataModel().read("Z_XF_LOGIN?$filter=user eq '"+user+"' and pwd eq '"+pwd+"'", {success: success, error: error, async: true});
    },

    getCustomersList : function (req, success, error)
    {
      //req is an object containing parameters passed to Odata then It's built the url
      var url  ="GetCustomerSet";
      var isFirst = true;
      for(var prop in req)
      {
        if(_.isEmpty(req[prop]))
          continue;

        if(isFirst)
        {
          url = url.concat("?$filter=");
          isFirst=false;
        }
        else {
          url = url.concat(" and ");
        }


        var propString = prop + " eq '"+req[prop]+"'";
        url= url.concat(propString);
      }
      this.getOdataModel().read(url, {success:success, error:error, async:true});
    }


};

//{
// 	'Uname': 'TPANICHI',
//     'Ustyp' : 'AM' ,
//     'Bukrs' : 'SI01',
//     'Vkorg' : 'SA20',
//     'Vtweg' : 'DO',
//     'Spart' : 'RI',
//     'Vkbur' : 'C002',
//     'Vkgrp' : 'C27'
// }
