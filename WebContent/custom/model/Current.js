jQuery.sap.declare("model.Current");
jQuery.sap.require("model.persistence.Storage");

//module to store and recover current entities

model.Current = (function () {

  var ticket;

  var setCurrentTicket = function (t) {
    ticket = t;
  };

  var getCurrentTicket = function () {
    if (!ticket) {
      var ticketData = model.persistence.Storage.session.get("currentTicket");
      if (ticketData) {
        ticket = new model.Ticket(ticketData);
      } else {
        ticket = null;
      }
    }
    return ticket;
  };


  var removeTicket = function () {
    model.persistence.Storage.session.remove("currentTicket");
    ticket = undefined;
  };



  return {
    setCurrentTicket: setCurrentTicket,
    getCurrentTicket: getCurrentTicket,
    removeTicket: removeTicket
  };
})();
