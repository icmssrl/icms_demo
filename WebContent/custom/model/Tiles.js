jQuery.sap.declare("model.Tiles");
jQuery.sap.require("model.i18n");


model.Tiles = function () {



    TileCollection = [
        {
            "icon": "timesheet",
            "title": model.i18n._getLocaleText("TICKET_MANAGEMENT"),
            "url": "noDataSplitDetail",
            "type": ["user","admin"]
     },
//        {
//            "icon": "timesheet",
//            "title": model.i18n._getLocaleText("TICKET_MANAGEMENT"),
//            "url": "noDataSplitDetail",
//            "type": "admin"
//     },
        {
            "icon": "manager-insight",
            "title": model.i18n._getLocaleText("TICKET_REPORT"),
            "url": "ticketReport",
            "type": ["admin"]
     },
        {
            "icon": "sys-help",
            "title": model.i18n._getLocaleText("HELP_DESK"),
            "url": "helpDesk",
            "type": ["user"]
     }

  ];

    //  societyTiles = [
    //    {
    //      "textColor":"lightgreen",
    //      "backgroundColor":"white",
    //      "icon": "./custom/img/loghi/beretta.gif",
    //      "title": "Beretta",
    //      "url": "launchpad",
    //      "society": "SI41"
    //    },
    //    {
    //      "textColor":"red",
    //      "backgroundColor":"white",
    //      "icon": "./custom/img/loghi/riello.gif",
    //      "title": "Riello Trade",
    //      "url": "launchpad",
    //      "society": "SI01"
    //    },
    //    {
    //        
    //      "textColor":"lightgreen",
    //      "backgroundColor":"white",
    //      "icon": "./custom/img/loghi/beretta.gif",
    //      "title": "Beretta",
    //      "url": "launchpad",
    //      "society":"CF"
    //    }
    //
    //  ];

    getTiles = function (profile) {
        var arrayTiles = [];
        for (var i = 0; i < TileCollection.length; i++) {
            var types = TileCollection[i].type;
            for (var t = 0; t < types.length; t++) {
                if (types[t] === profile)
                    arrayTiles.push(TileCollection[i])
            };
        };
        return arrayTiles;
    };

//    getTiles = function (type) {
//
//        arrayTiles = _.where(TileCollection, {
//            'type': type
//        });
//        return arrayTiles;
//
//    };

    //  getSocietyTile = function(society)
    //  {
    //    return _.find(societyTiles, {society : society});
    //  };

    return {
        getMenu: getTiles

    };



}();
