jQuery.sap.require("model.User");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageToast");

view.abstract.AbstractController.extend("view.Login", {

  onExit: function () {

  },


  onInit: function () {
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    this.credentialModel = new sap.ui.model.json.JSONModel();

  },


  handleRouteMatched: function (evt) {

    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    var name = evt.getParameter("name");

    if (name !== "login") {
      return;
    }

    this.user = new model.User();
    //var credentialModel = this.user.getNewCredential();
    this.getView().setModel(this.credentialModel, "usr");

    model.i18n.setModel(this.getView().getModel("i18n"));






  },
 
  


//  onLoginPress: function (evt) {
////  onLoginPress: function (evt) {
////    var username = this.getView().getModel("usr").getProperty("/username");
//
////    var pwd = this.getView().getModel("usr").getProperty("/password");
////
////    this.user.setCredential(username, pwd);
////    var doLogin = _.bind(this.user.doLogin, this);
//
////    var choosePath = _.bind(this.choosePath, this);
////
////    this.chiamataOData(username, pwd)
////      .then(doLogin(this.user.getCredential().username, this.user.getCredential().pwd)
////        .then(_.bind(
////          function (result) {
////
//////            if (result.organizationData && _.chain(result.organizationData.results).pluck('division').uniq().value().length === 1) {
//////              this.router.navTo("launchpad");
//////            } else {
//////              if (_.chain(result.organizationData.results).pluck('division').uniq().value().length > 1) {
//////                this.router.navTo("soLaunchpad");
//////              }
//////            }
//////          }
////          this.router.navTo("soLaunchpad");
////          }, this)));
//        this.router.navTo("pmScheduling");
//
//  },

 onAfterRendering : function(evt)
	{
			view.abstract.AbstractController.prototype.onAfterRendering.apply(this, arguments);
		$(document).on("keypress", _.bind(function(evt)
	{
		if(evt.keyCode === 13 && (this.validateCheck()))  
		{
			this.onLoginPress();
		}

	}, this));



	},

	onLoginPress:function(evt)
	{
    
    //&this.chiamataOData();
        var username = this.getView().getModel("usr").getProperty("/username");
		var pwd = this.getView().getModel("usr").getProperty("/password");
		
		this.user.setCredential(username, pwd);

		 this.user.doLogin()
		 .then(_.bind(function(result){
		 	this.router.navTo("launchpad");
		
		 }, this));




	},

  
//  chiamataOData: function (user, pwd) {
//    var defOdata = Q.defer();
//    fError = function () {
//      sap.m.MessageBox.alert("Utente SAP non trovato", {
//        title: "Attenzione"
//      });
//    };
//    fSuccess = function () {
//      defOdata.resolve();
//      console.log("credenziali");
//    };
//
//    var success = _.bind(fSuccess, this);
//    var error = _.bind(fError, this);
//
//    var tok = "SYSTEM" + ':' + "Saphdb_40$";
//    //var tok = user + ":" + pwd;
//    var auth = btoa(tok);
//
//    $.ajaxSetup({
//      headers: {
//        "Authorization": "Basic " + auth,
//        "X-CSFR-TOKEN": "fetch"
//      }
//    });
//    var url = "http://gspidev.icms.it:8000/GSPI/odata/odata.xsodata";
//    $.ajax({
//      url: url,
//      type: "GET",
//      success: success,
//      error: error,
//      async: false
//    });
//
//    return defOdata.promise;
//  },
  

});