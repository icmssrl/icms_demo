jQuery.sap.require("utils.Formatter");
//jQuery.sap.require("model.Periods");
//jQuery.sap.require("model.Priority");
//jQuery.sap.require("model.Projects");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("sap.m.MessageBox");

view.abstract.AbstractController.extend("view.HelpDesk", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");


    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "helpDesk") {
            return;
        }

        console.log(name)
    },

    navBack: function (evt) {
//        this.router.myNavBack();
        this.router.navTo("launchpad");
    },

    setProductTypeFromSegmented: function (evt) {
        var testo = evt.getParameter("button").getProperty("text");
        console.log(testo)
    },

    button1Press: function (evt) {
        var testo = evt.getSource().getProperty("text");
        //        console.log(testo);
    },

    wizardCompletedHandler: function (evt) {
        this.getView().byId("CreateProductWizard").discardProgress(this.getView().byId("ProductTypeStep"));
        sap.m.MessageBox.show(
            'Tiket inviato',
            sap.m.MessageBox.Icon.INFORMATION,
            "info", [sap.m.MessageBox.Action.OK],
            _.bind(this._okPress, this)
        );
        //        this.router.navTo("launchpad");
    },

    _okPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.OK) {
            this.router.navTo("launchpad");
        }
    },

});
