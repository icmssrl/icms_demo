jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Tickets");
jQuery.sap.require("model.Ticket");
jQuery.sap.require("model.collections.Partners");
jQuery.sap.require("model.Partner");
jQuery.sap.require("model.Current");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("model.i18n");

view.abstract.AbstractController.extend("view.TicketDetail", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        
        // Sets the text to the label
//        this.getView().byId("UploadCollection").addEventDelegate({
//            onBeforeRendering : function () {
//                this.getView().byId("attachmentTitle").setText(this.getAttachmentTitleText());
//            }.bind(this)
//        });

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "ticketDetail"){
			return;
		}
        
        if(this.addPartnersDialog){
            this.addPartnersDialog.destroy();
        }

		this.id = evt.getParameters().arguments.id;
        this.appModel = this.getView().getModel("appStatus");
//        if(this.appModel.getProperty("/setEditable"))
        this.appModel.setProperty("/setEditable", false);
        
        var iconTabBar = this.getView().byId("iconTabBarId");
        iconTabBar.setSelectedKey("description");
        if(iconTabBar.getSelectedKey() !== "status"){
            var switchModeButton = this.getView().byId("editButton");
            var assignStatusButton = this.getView().byId("assignStatusButton");
//            var confirmedButton = this.getView().byId("confirmedButton");
//            var assignButton = this.getView().byId("assignButton");
            switchModeButton.setVisible(false);
            assignStatusButton.setVisible(false);
//            confirmedButton.setVisible(false);
//            assignButton.setVisible(false);
        }
        
        this.user = model.persistence.Storage.session.get("user");
        this.userModel = new sap.ui.model.json.JSONModel(this.user);
        this.getView().setModel(this.userModel, "userModel");
        
        
        model.collections.Tickets.getById(this.id)
		.then(_.bind(function(res){
            console.log(res);
            this.ticket = res;
            this.refreshView(res);

        }, this));
 
        
        this.populateSelect();
             
          

	},
    
	
	refreshView : function(data)
	{
        this.getView().setModel(data.getModel(), "t");

	},
    
    refreshPartnerList: function(data){
        this.partnerListModel = new sap.ui.model.json.JSONModel();
//        var partnersArr = this.ticket.associatedPartners;
   
//        data = data.filter(_.bind(function(val) {
//          return this.ticket.associatedPartners.indexOf(val) == -1;
//        }, this));
        var p = {partners:[]};
        p.partners = data;
        this.partnerListModel.setData(p);
        this.getView().setModel(this.partnerListModel, "p");
    },
    
    populateSelect: function () {
    var pToSelArr = [
      {
        "type": "priorityTypes",
        "namespace": "priorityTypes"
      }
    ];

    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.getModel(item.type)
        .then(_.bind(function (result) {
          this.getView().setModel(result, item.namespace);

        }, this))
    }, this));
  },
    

    onPost: function (oEvent) {
			var oDate = new Date();
			var sDate = utils.ParseDate().formatDate(oDate, "dd/MM/yyyy");
			// create new entry
			var sValue = oEvent.getParameter("value");
			var oEntry = {
				Author : this.user.username,
				AuthorPicUrl : "custom/img/logo.png",
				Type : "Ticket Type: " + this.getView().getModel("t").getData().ticketType,
				Date : "" + sDate,
				Text : sValue
			};
 
			// update model
			var oModel = this.getView().getModel("t");
			var aEntries = oModel.getData().notes;
			aEntries.unshift(oEntry);
			oModel.refresh();
		},
 
    onSenderPress: function (oEvent) {
			sap.m.MessageToast.show("Clicked on Link: " + oEvent.getSource().getSender());
	},
 
    onIconPress: function (oEvent) {
			sap.m.MessageToast.show("Clicked on Image: " + oEvent.getSource().getSender());
	},
    
    
    onIconTabClicked: function(event){
        var switchModeButton = this.getView().byId("editButton");
        var assignStatusButton = this.getView().byId("assignStatusButton")

        var source = event.getSource();
        var selectedKey = source.getSelectedKey();
        if(selectedKey === "status"){
            if(this.appModel.getProperty("/setEditable")===true){
                switchModeButton.setVisible(false);
                assignStatusButton.setVisible(true);

            }else{
                switchModeButton.setVisible(true);
                assignStatusButton.setVisible(false);

            }
            
        }else{
            switchModeButton.setVisible(false);
            assignStatusButton.setVisible(false);

            
        }
    },
    
    switchToEditMode: function(evt){
        var source = evt.getSource();
        var switchModeButton = this.getView().byId("editButton");
        var assignStatusButton = this.getView().byId("assignStatusButton");
        this.appModel.setProperty("/setEditable", true);
        switchModeButton.setVisible(false);
        assignStatusButton.setVisible(true);
        this.getView().getModel("t").refresh();
    },
    
    returnToViewMode: function(){
        var switchModeButton = this.getView().byId("editButton");
        var assignStatusButton = this.getView().byId("assignStatusButton");
        this.appModel.setProperty("/setEditable", false);
        switchModeButton.setVisible(true);
        assignStatusButton.setVisible(false);
        this.getView().getModel("t").refresh();
        sap.m.MessageToast.show("Stato cambiato");
    },
    
    onOpenAssignActionSheetPress: function(oEvent){
       
			var oButton = oEvent.getSource();
            var nextStates = this.ticket.nextStates;
            var buttons = _.bind(this.createStatusButtons, this);
            var button;
			// create action sheet only once
			if (!this._actionSheet) {
				this._actionSheet = sap.ui.xmlfragment(
					"view.fragment.assignStatusActionSheet",
					this
				);
				this.getView().addDependent(this._actionSheet);
			}
            if(this._actionSheet.getButtons().length > 0) 
            this._actionSheet.removeAllButtons();
            for(var i = 0; i < nextStates.length; i++){
                button = buttons(nextStates[i]);
                this._actionSheet.addButton(button);
            }

			this._actionSheet.openBy(oButton);
  
    },
    
    createStatusButtons: function(b){
        var newButton = new sap.m.Button(
            {
                text: b.text,
                class: "assignStatusButtonClass",
                press: [this.assignNextStatusButton, this]
            }
        );
        return newButton;
    },
    
    assignNextStatusButton: function(evt){
        var source = evt.getSource();
        var newStatus = source.getText();
        this.ticket.setNewStatus(newStatus);
        var data = this.getView().getModel("t").getData();
        this.ticket.update(data);
        this.returnToViewMode();
    },
    
    handleRemovePartner: function(evt){
        var position = evt.getParameters().listItem;
        var oContext = position.getBindingContext("t");
        var oModel = oContext.getModel();
        var row = oContext.getObject();
//        var positionId = row.getId();
//        this.order.removePosition(positionId);
        var oData = oModel.getData();
        var currentRowIndex = parseInt(oContext.getPath().split('/')[oContext.getPath().split('/').length - 1]);
        oData.associatedPartners.splice(currentRowIndex,1);
        oModel.updateBindings();
        this.getView().getModel("t").refresh(true);
    },
    
    keyUpFunc: function(e) {
          if (e.keyCode == 27) {
                     // codice per il pulsante escape per evitare che lo user chiuda il dialog via ESC
                    
                    if(this.addPartnersDialog){
                            this.addPartnersDialog.destroy();
                    }
                    $(document).off("keyup");
//                    this.router.navTo("launchpad");
                    }
    },
    
    openAddPartnerDialog: function(evt){
        this.addPartnersDialog = sap.ui.xmlfragment("view.dialog.addPartnersDialog", this);
        var page = this.getView().byId("detailPageId");
        page.addDependent(this.addPartnersDialog);
        model.collections.Partners.loadPartners() 
        .then(_.bind(function(res){
            console.log(res);
            this.refreshPartnerList(res);
            this.addPartnersDialog.open();
        }, this));
        

        $(document).keyup(_.bind(this.keyUpFunc, this));
    },
    
    handleSearchOnPartnerDialog: function(oEvent){
        var sValue = oEvent.getParameter("value");
        var oFilter = new sap.ui.model.Filter("partnerName", sap.ui.model.FilterOperator.Contains, sValue);
        var oBinding = oEvent.getSource().getBinding("items");
        oBinding.filter([oFilter]);
    },
    
    handleConfirmOnPartnerDialog: function(oEvent){
        var aContexts = oEvent.getParameter("selectedContexts");
        var partners= [];
        if (aContexts.length) {
            sap.m.MessageToast.show(model.i18n._getLocaleText("PARTNER_CHOSEN") + " : " + aContexts.map(function(oContext) { 
                partners.push(oContext.getObject());
                return oContext.getObject().partnerName; }));


        }else{
            sap.m.MessageToast.show(model.i18n._getLocaleText("NO_PARTNER_CHOSEN"));
        }
        oEvent.getSource().getBinding("items").filter([]);
        if(partners.length>0){
            this.ticket.addNewPartners(partners);
            this.getView().getModel("t").refresh();
        }
        this.addPartnersDialog.destroy();
       
    },
    
    handleCloseOnPartnerDialog: function(evt){
        if(this.addPartnersDialog){
            this.addPartnersDialog.destroy();
        }
        sap.m.MessageToast.show(model.i18n._getLocaleText("NO_PARTNER_CHOOSEN")); 
       
    },
	
	
    
    onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");  
    },
    
    
    
    ////////////**************************************///////////////////////////////////
    ////////////Funzioni che gestiscono gli upload
    
    
    formatAttribute: function (sValue) {
    jQuery.sap.require("sap.ui.core.format.FileSizeFormat");
    if (jQuery.isNumeric(sValue)) {
      return sap.ui.core.format.FileSizeFormat.getInstance({
        binaryFilesize: false,
        maxFractionDigits: 1,
        maxIntegerDigits: 3
      }).format(sValue);
    } else {
      return sValue;
    }
  },

  onChange: function (oEvent) {
    var oUploadCollection = oEvent.getSource();
    var filelist = [];
    filelist.push(oEvent.getParameters().files[0]);
    // Header Token
    var oCustomerHeaderToken = new sap.m.UploadCollectionParameter({
      name: "x-csrf-token",
      value: "securityTokenFromModel"
    });
    oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
  },

  onFileDeleted: function (oEvent) {
    var oData = this.getView().byId("UploadCollection").getModel("t").getData();
    if (oData.docs) {
      var aItems = jQuery.extend(true, {}, oData).docs;
    } else {
      var aItems = [];
    }

    var sDocumentId = oEvent.getParameter("documentId");
    jQuery.each(aItems, function (index) {
      if (aItems[index] && aItems[index].documentId === sDocumentId) {
        aItems.splice(index, 1);
      };
    });
    this.getView().byId("UploadCollection").getModel("t").refresh();
    var oUploadCollection = oEvent.getSource();
    oUploadCollection.setNumberOfAttachmentsText("Uploaded (" + oUploadCollection.getItems().length + ")");
    sap.m.MessageToast.show("FileDeleted event triggered.");
  },

  onFileRenamed: function (oEvent) {
    var oData = this.getView().byId("UploadCollection").getModel().getData();
    if (oData.docs) {
      var aItems = jQuery.extend(true, {}, oData).docs;
    } else {
      var aItems = [];
    }
    var sDocumentId = oEvent.getParameter("documentId");
    jQuery.each(aItems, function (index) {
      if (aItems[index] && aItems[index].documentId === sDocumentId) {
        aItems[index].fileName = oEvent.getParameter("item").getFileName();
      };
    });
    this.getView().byId("UploadCollection").getModel("t").refresh();
    sap.m.MessageToast.show("FileRenamed event triggered.");
  },

  onFileSizeExceed: function (oEvent) {
    sap.m.MessageToast.show("FileSizeExceed event triggered.");
  },

  onTypeMissmatch: function (oEvent) {
    sap.m.MessageToast.show("TypeMissmatch event triggered.");
  },

  onUploadComplete: function (oEvent) {
    var oData = this.getView().byId("UploadCollection").getModel("t").getData();
    if (oData.docs) {
      var aItems = jQuery.extend(true, {}, oData).docs;
    } else {
      var aItems = [];
    }
    var oItem = {};
    var sUploadedFile = oEvent.getParameter("files")[0].fileName;
    // at the moment parameter fileName is not set in IE9
    if (!sUploadedFile) {
      var aUploadedFile = (oEvent.getParameters().getSource().getProperty("value")).split(/\" "/);
      sUploadedFile = aUploadedFile[0];
    }
    oItem = {
      "documentId": jQuery.now().toString(), // generate Id,
      "fileName": sUploadedFile,
      "mimeType": "",
      "thumbnailUrl": "",
      "url": "",
      "attributes": [
        {
          "title": "Uploaded By",
          "text": model.persistence.Storage.session.get("user").username,
						},
        {
          "title": "Uploaded On",
          "text": new Date(jQuery.now()).toLocaleDateString()
						},
        {
          "title": "File Size",
          "text": "505000"
						}
					]
    };
    //aItems.unshift(oItem);
    oData.docs.unshift(oItem);
    this.getView().byId("UploadCollection").getModel("t").refresh();
    var oUploadCollection = oEvent.getSource();
    oUploadCollection.setNumberOfAttachmentsText("Uploaded (" + oUploadCollection.getItems().length + ")");
    // delay the success message to notice onChange message
    setTimeout(function () {
      sap.m.MessageToast.show("UploadComplete event triggered.");
    }, 4000);
  },

  onSelectChange: function (oEvent) {
    var oUploadCollection = sap.ui.getCore().byId(this.getView().getId() + "--UploadCollection");
    oUploadCollection.setShowSeparators(oEvent.getParameters().selectedItem.getProperty("key"));
  },
  onBeforeUploadStarts: function (oEvent) {
    // Header Slug
    var oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
      name: "slug",
      value: oEvent.getParameter("fileName")
    });
    oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
    sap.m.MessageToast.show("BeforeUploadStarts event triggered.");
  },
  onUploadTerminated: function (oEvent) {
    // get parameter file name
    var sFileName = oEvent.getParameter("fileName");
    // get a header parameter (in case no parameter specified, the callback function getHeaderParameter returns all request headers)
    var oRequestHeaders = oEvent.getParameters().getHeaderParameter();
  }
    
    
    ///////////******************************************//////////////////////////////


});
